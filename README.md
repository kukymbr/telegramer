# Telegamer

Package to access Telegram Bot API from the php.

## Requirements

Telegramer requires PHP 7.0+.

## Installation

The supported way of installing Telegramer is via Composer.

```bash
$ composer require kukymbr/telegramer
```

## Usage

```php
<?php

use \Kukymbr\Telegramer\Connection;
use \Kukymbr\Telegramer\Bot;
use \Kukymbr\Telegramer\Response;
use \Kukymbr\Telegramer\Notifier;

$connection = new Connection();
// You can set your cURL options here.
// For example, proxy settings.
$connection->setOptArray([]);

$bot = new Bot('12345:YourBotToken', $connection);

// Send a request to API method via magic
$me = $bot->getMe();

/**
 * @var Response $me
 */
if ($me->isOk()) {
    // Do something with the result 
    var_dump($me->result());
}

$notifier = new Notifier($bot);
$notifier->addChat(123456);

$notifier->send('Some message from the site');
$notifier->sendMap(
    [
        'New request #123',
        'name' => 'John',
        'phone' => '+123456789'
    ],
    [
        'name' => 'User name',
        'phone' => 'User phone'    
    ]
);
```

## License

The MIT License (MIT). Please see License File for more information.