<?php

namespace Kukymbr\Telegramer\Connection;

/**
 * Class Exception
 *
 * @package Kukymbr\Telegramer\Connection
 */
class Exception extends \Kukymbr\Telegramer\Exception
{
}