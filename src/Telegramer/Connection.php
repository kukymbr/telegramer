<?php

namespace Kukymbr\Telegramer;

use Kukymbr\CurlWrapper\Curl;
use Kukymbr\Telegramer\Connection\Exception;

/**
 * Class Connection
 *
 * @package Kukymbr\Telegramer
 */
class Connection extends Curl
{
    /**
     * Fetch as Response
     *
     * @param string|null $url
     * @param array|null $getParams
     * @return Response
     * @throws Exception
     */
    public function fetchResponse(string $url = null, array $getParams = null) :Response
    {
        $data = $this->_fetchJson($url, $getParams);

        return new Response($data);
    }

    /**
     * Fetch response from cURL handler
     *
     * @param string|null $url
     * @param array|null $getParams
     * @return string
     * @throws Exception
     */
    protected function _fetch(string $url = null, array $getParams = null) :string
    {
        $response = $this->exec($url, $getParams);

        if ($response === false && ($err = $this->getError($errText))) {
            throw new Exception(
                'Failed to load data from URL: error #' . $err . ' ' . $errText,
                417
            );
        }

        $responseCode = $this->getResponseCode();

        if ($responseCode >= 400) {
            throw new Exception(
                ($response ? $response : 'URL responses with error code ' . $responseCode),
                $responseCode
            );
        }

        return $response;
    }

    /**
     * Fetch JSON-decoded data from cURL handler
     *
     * @param string|null $url
     * @param array|null $getParams
     * @return mixed
     * @throws Exception
     */
    protected function _fetchJson(string $url = null, array $getParams = null)
    {
        $response = $this->_fetch($url, $getParams);

        $data = json_decode($response);

        if ($data === null) {
            $errCode = json_last_error();
            $errText = json_last_error_msg();
            if ($errCode) {
                throw new Exception(
                    'Failed to decode JSON (err ' . $errCode . '): ' . $errText,
                    422
                );
            }
        }

        return $data;
    }

    /**
     * Get default cURL options.
     *
     * @return array
     */
    protected function _getDefaultOptions(): array
    {
        return [
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_AUTOREFERER     => true,
            CURLOPT_FAILONERROR     => true,
            CURLOPT_FOLLOWLOCATION  => false,
        ];
    }
}