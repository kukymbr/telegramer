<?php

namespace Kukymbr\Telegramer;

use \Kukymbr\Telegramer\Response\Exception;

/**
 * Class Response
 *
 * @package Kukymbr\Telegramer
 */
class Response
{
    /**
     * Response data
     * @var \StdClass
     */
    protected $_data;

    /**
     * Response constructor.
     *
     * @param \StdClass $responseData
     */
    public function __construct(\StdClass $responseData)
    {
        $this->_data = $responseData;
    }

    /**
     * Is OK?
     *
     * @param bool $throwIfNot
     * @return bool
     * @throws Exception
     */
    public function isOk(bool $throwIfNot = false) :bool
    {
        $res = $this->_data->ok;

        if (!$res && $throwIfNot) {
            throw new Exception($this->errorMsg());
        }

        return $res;
    }

    /**
     * Get result data object
     *
     * @return \StdClass
     * @throws Exception
     */
    public function result() :\StdClass
    {
        $this->isOk(true);

        return $this->_data->result;
    }

    /**
     * Get result value
     *
     * @param string $key
     * @param null $default
     * @return null
     * @throws Exception
     */
    public function get(string $key, $default = null)
    {
        $res = $this->result();

        return ($res->{$key} ?? $default);
    }

    /**
     * Get error message if error
     *
     * @return string
     */
    public function errorMsg() :string
    {
        if ($this->_data->ok) {
            return '';
        }

        if ($this->_data->description) {
            return $this->_data->description;
        }

        return 'API request failed';
    }
}