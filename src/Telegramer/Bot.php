<?php

namespace Kukymbr\Telegramer;

/**
 * Class Bot
 *
 * @package Kukymbr\Telegramer
 */
class Bot
{
    /**
     * Requests' URL base
     */
    const REQUEST_URL_BASE = 'https://api.telegram.org/bot';

    /**
     * Bot's token
     * @var string
     */
    protected $_token;

    /**
     * Connection instance
     * @var Connection
     */
    protected $_connection;

    /**
     * Bot constructor.
     *
     * @param string $token
     * @param Connection|null $connection
     * @throws \Kukymbr\CurlWrapper\Exception
     */
    public function __construct(string $token, Connection $connection = null)
    {
        if ($connection === null) {
            $connection = new Connection();
        }

        $this->_token = $token;
        $this->_connection = $connection;
    }

    /**
     * Send API request by magic.
     * For example:
     * $bot->getMe();
     * $bot->sendMessage(['chat_id' => 1, 'text' => 'hello']);
     *
     * @param string $method
     * @param array $arguments
     * @return Response
     * @throws Exception
     */
    public function __call(string $method, array $arguments) :Response
    {
        $params = reset($arguments);
        return $this->exec(
            $method,
            $params && is_array($params) ? $params : null
        );
    }

    /**
     * Request API method
     *
     * @param string $method
     * @param array|null $params
     * @return Response
     * @throws Connection\Exception
     */
    public function exec(string $method, array $params = null) :Response
    {
        $conn = $this->_connection;
        $url = $this->url($method);

        $conn->setOpt(CURLOPT_POST, true);
        $conn->setOpt(CURLOPT_POSTFIELDS, $params ?? '');

        return $conn->fetchResponse($url);
    }

    /**
     * Get bot's API URL
     *
     * @param string|null $method
     * @return string
     */
    public function url(string $method = null)
    {
        $url = static::REQUEST_URL_BASE . $this->_token;

        if ($method !== null) {
            $url .= '/' . $method;
        }

        return $url;
    }
}