<?php

namespace Kukymbr\Telegramer\Response;

/**
 * Class Exception
 *
 * @package Kukymbr\Telegramer\Response
 */
class Exception extends \Kukymbr\Telegramer\Exception
{
}