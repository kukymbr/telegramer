<?php

namespace Kukymbr\Telegramer;

/**
 * Class Exception
 *
 * @package Kukymbr\Telegramer
 */
class Exception extends \Exception
{
}