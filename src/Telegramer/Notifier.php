<?php

namespace Kukymbr\Telegramer;

/**
 * Class Notifier
 *
 * @package Kukymbr\Telegramer
 */
class Notifier
{
    /**
     * Sending Bot instance
     * @var Bot
     */
    protected $_bot;

    /**
     * Target chats IDs or usernames
     * @var array
     */
    protected $_chats = [];

    /**
     * Notifier constructor.
     *
     * @param Bot $bot
     * @param array|int|string $chats
     * @throws Exception
     */
    public function __construct(Bot $bot, $chats = null)
    {
        $this->_bot = $bot;

        if ($chats !== null) {
            if (!is_array($chats)) {
                $chats = [$chats];
            }
            foreach ($chats as $chat) {
                $this->addChat($chat);
            }
        }
    }

    /**
     * Add target chat ID or username
     *
     * @param int|string $chatId
     * @return Notifier
     * @throws Exception
     */
    public function addChat($chatId) :Notifier
    {
        if (empty($chatId)) {
            throw new Exception('Empty chat ID or username given', 400);
        }

        if (is_numeric($chatId)) {
            $chatId = (int)$chatId;
        } elseif (is_string($chatId)) {
            if (strpos($chatId, '@') !== 0) {
                throw new Exception('Username string must be leaded by @ symbol', 400);
            }
        } else {
            throw new Exception('Cannot use ' . gettype($chatId) . ' as a chat ID', 400);
        }

        if (!in_array($chatId, $this->_chats, true)) {
            $this->_chats[] = $chatId;
        }

        return $this;
    }

    /**
     * Remove target chat from the list
     *
     * @param $chatId
     * @return Notifier
     */
    public function removeChat($chatId) :Notifier
    {
        foreach ($this->_chats as $key => $chat) {
            if ($chat === $chatId) {
                unset($this->_chats[$key]);
            }
        }

        return $this;
    }

    /**
     * Clear target chats list
     *
     * @return Notifier
     */
    public function clearChats() :Notifier
    {
        $this->_chats = [];

        return $this;
    }

    /**
     * Send message to target chats.
     * Returns responses in format:
     *
     * (object)[
     *  'done' => [<Response1>, ...],
     *  'fail' => [<ResponseN>, ...]
     * ]
     *
     * @param string $message
     * @param bool $isHtml
     * @param array|null $additional
     * @return \StdClass
     * @throws Response\Exception
     */
    public function send(string $message, bool $isHtml = false, array $additional = null) :\StdClass
    {
        $options = [
            'text' => $message,
        ];

        if ($isHtml) {
            $options['parse_mode'] = 'html';
        }

        if ($additional !== null) {
            $options += $additional;
        }

        $res = (object)[
            'done' => [],
            'fail' => []
        ];

        foreach ($this->_chats as $chatId) {
            /**
             * @var Response $resp
             */
            $resp = $this->_bot->sendMessage(
                ['chat_id' => $chatId] + $options
            );
            if ($resp->isOk()) {
                $res->done[] = $resp;
            } else {
                $res->fail[] = $resp;
            }
        }

        return $res;
    }

    /**
     * Prepare & send key=>value map.
     * You may define keys' labels in $labels arg in format
     * [$key => $label]
     *
     * @param array $map
     * @param array $labels
     * @param array|null $additional
     * @return \StdClass
     * @throws Response\Exception
     */
    public function sendMap(array $map, array $labels = [], array $additional = null) :\StdClass
    {
        $message = [];

        foreach ($map as $key => $value) {
            if (is_int($key)) {
                $message[] = $value;
            } else {
                $message[] = '<b>' . ($labels[$key] ?? $key) . '</b>: ' . $value;
            }
        }

        $message = implode("\n", $message);

        return $this->send($message, true, $additional);
    }
}